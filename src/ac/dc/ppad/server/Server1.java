package ac.dc.ppad.server;

import java.io.Serializable;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Server1 implements InterfaceInfo, Serializable {

	private static final long serialVersionUID = 6488919630047609369L;
	
	public Server1() {
		
		super();
	}

	public static void main(String[] args) {
		
		System.setProperty("java.security.policy","file:./server.policy");
		
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
		}
		
		try {

			String name = "//127.0.0.1:1080/myServer";
			Server1 server = new Server1();
			InterfaceInfo stub = (InterfaceInfo) UnicastRemoteObject.exportObject(server, 0);
			Naming.rebind(name, stub);
			System.out.println("Server1 bound");

		} catch (Exception e) {

			System.err.println("Server1 exception: ");
			e.printStackTrace();
		}
		System.out.println("Server running...");
	}

	@Override
	public String getSomething(String source) throws RemoteException {

		return "!!!" + source + "!!!";
	}

}
